package com.glearning.lab;

public class SearchElement {

	public static void main(String[] args) {
		/*
		 * 1. Accept input from the user and create an array 2. Sort the array
		 * (Quick/Merge) 3. Left rotate 4. Search for the element
		 */
	}

	/*
	 * Merge sort -> merges two subarrays of array[]
	 * 
	 * First subarray is [startindex .. midIndex] Second subarray is [midIndex+1 ..
	 * endIndex] base condition startIndex <= endIndex
	 */

	void merge(int array[], int startIndex, int midIndex, int endIndex) {

		int size1 = midIndex - startIndex + 1;
		int size2 = endIndex - midIndex;
		
		int left[] = new int[size1];
		int right[] = new int[size2];
		
		/* copy the data to the temp arrays */
		for(int index = 0; index < size1; index++) {
			left[index] = array[startIndex + index];
		}
		
		for(int index = 0; index < size2; index++) {
			right[index] = array[midIndex + 1 + index];
		}
		
		

	}

	/*
	 * dividing the array and then calling the merge method
	 */
	void mergeSort(int[] array, int startIndex, int endIndex) {

		if (startIndex < endIndex) {
			//find the midIndex
			int midIndex = startIndex + (endIndex - startIndex) / 2;
			
			//divide the array into two sub-arrays
			mergeSort(array, startIndex, midIndex);
			mergeSort(array, midIndex + 1, endIndex);
			
			//merging of the two halves
			merge(array, startIndex, midIndex, endIndex);
		}
	}

}
